<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Models\Api\Cuenta;
use App\Models\Api\Prestamo;
use Illuminate\Http\Request;
use App\Models\Api\HistorialPago;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class PrestamoController extends Controller
{
    public function getPrestamo($prestamo_id)
    {
        //Obtenemos información del prestamo.
        $prestamo = Prestamo::where('user_id', Auth::user()->id)->where('prestamos.id', $prestamo_id)->first();

        //Obtenemos los pagos asociados al prestamo en caso de tener.
        $pagos = HistorialPago::where('user_id', Auth::user()->id)->where('prestamo_id', $prestamo->id)->get();

        //Retornamos la información del prestamo y si tiene pagos asociados.
        return response()->json([
            'prestamo' => $prestamo,
            'pagos' => $pagos
        ], 201);
    }

    public function setPrestamo(Request $request, $cuenta_id)
    {
        //Validación del formulario.
        $validation =  Validator::make($request->all(), [
            'saldo_prestamo' => ['required'],
        ]);

        //Si hay algún error en la validación, retorna los errores.
        if ($validation->fails()) {
            return response()->json($validation->errors(), 500);
        }

        //Consulta si la cuenta no tiene un prestamo activo.
        $prestamo = Prestamo::where('cuenta_id', $cuenta_id)
            ->where('status', true)
            ->where('user_id', Auth::user()->id)
            ->first();

        //Si no tiene un prestamo, genera uno nuevo.
        if (!$prestamo) {
            //Generamos el nuevo prestamo
            Prestamo::create([
                'user_id' => Auth::user()->id,
                'cuenta_id' => $cuenta_id,
                'saldo_prestamo' => $request->saldo_prestamo,
                'fecha_prestamo' => date('Y-m-d')
            ]);

            //Abonamos el prestamo a la cuenta.
            $cuenta = Cuenta::find($cuenta_id);

            //Sumamos el saldo actual más el prestamo.
            $nuevo_saldo = $cuenta->saldo_cuenta + $request->saldo_prestamo;

            //Actualizamos el saldo de la cuenta.
            $cuenta->update([
                'saldo_cuenta' => $nuevo_saldo
            ]);

            //Retorna el mensaje satisfactorio.
            return response()->json([
                'message' => '¡Prestamo solicitado correctamente! El saldo será abonado a su cuenta.'
            ], 201);
        }

        //Si tiene un prestamo activo, envia el mensaje.
        return response()->json([
            'message' => '¡Ya tiene un prestamo activo!'
        ], 403);
    }
}
