<?php

namespace App\Http\Controllers\Api\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ResetPasswordController extends Controller
{
    public function resetPassword(Request $request)
    {
        //Validación del formulario.
        $validation =  Validator::make($request->all(), [
            'email' => ['required', 'email'],
            'answer' => ['required'],
            'new_password' => ['required', 'max:8']
        ]);

        //Si hay algún error en la validación, retorna los errores.
        if ($validation->fails()) {
            return response()->json($validation->errors());
        }

        //Consultamos en la tabla si el correo de usuario existe y que la respuesta de seguridad es correcta.
        $user = User::where('email', $request->email)
            ->where('answer', $request->answer)
            ->first();

        //Si los datos son correctos, se guarda la nueva contraseña.
        if(isset($user->id)) {
            $user->update([
                'password' => Hash::make($request->new_password)
            ]);

            //Retornamos un mensaje de éxito.
            return response()->json([
                'message' => '¡Contraseña cambiada correctamente!'
            ], 201);
        }

        //Si el correo o la respuesta de seguridad son incorrectos, retornamos el siguiente mensaje.
        return response()->json([
            'message' => '¡La respuesta de seguridad o correo ingresado es incorrecto!'
        ], 401);
    }
}
