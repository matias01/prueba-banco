<?php

namespace App\Http\Controllers\Api\Auth;

use App\Models\User;
use App\Models\Api\Cuenta;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        //Validación del formulario.
        $validation =  Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:80'],
            'email' => ['required', 'email'],
            'password' => ['required'],
            'answer' => ['required', 'string', 'max:120'],
        ]);

        //Si hay algún error en la validación, retorna los errores.
        if ($validation->fails()) {
            return response()->json($validation->errors(), 500);
        }

        //Se crea el usuario nuevo.
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'answer' => $request->answer,
        ]);

        //Se genera el número de cuenta (solo para efectos de este ejemplo).
        Cuenta::create([
            'user_id' => $user->id,
            'numero_cuenta' => rand(),
            'tipo_cuenta' => 'Ahorro',
        ]);

        //Retornamos el mensaje de éxito.
        return response()->json([
            'message' => '¡Usuario creado correctamente!'
        ], 201);
    }

    public function login(Request $request)
    {
        //Validación del formulario.
        $validation =  Validator::make($request->all(), [
            'email' => ['required', 'email'],
            'password' => ['required']
        ]);

        //Si hay algún error en la validación, retorna los errores.
        if ($validation->fails()) {
            return response()->json($validation->errors(), 500);
        }

        //Se recibe las credenciales del formulario de login.
        $credentials = request()->only('email', 'password');

        //Validamos las credenciales.
        if (Auth::attempt($credentials)) {
            //Si las credenciales son correctas, tomamos la info del usuario.
            $user = Auth::getLastAttempted();

            //Creamos el token de autorización para las rutas privadas.
            $token = $user->createToken('auth_token')->plainTextToken;

            //Retornamos la respuesta en un JSON con la información del token, tipo y datos del usuario.
            return response()->json([
                'access_token' => $token,
                'token_type' => 'Bearer',
                'user' => $user
            ], 201);
        } else {
            //Si las credenciales son incorrectas retonar la siguiente respuesta.
            return response()->json([
                'message' => '¡No autorizado!'
            ], 404);
        }
    }

    public function logout()
    {
        //Elimina el token del usuario autenticado.
        auth()->user()->tokens()->delete();

        //Retornamos el mensaje de que la sesión fue cerrada correctamente.
        return response()->json([
            'message' => '¡Sesión cerrada correctamente!'
        ], 201);
    }
}
