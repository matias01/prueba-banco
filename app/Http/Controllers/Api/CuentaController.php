<?php

namespace App\Http\Controllers\Api;

use App\Models\Api\Cuenta;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Api\Prestamo;
use Illuminate\Support\Facades\Auth;

class CuentaController extends Controller
{
    public function index()
    {
        //Listamos las cuentas asociadas al cliente autenticado.
        $cuentas = Cuenta::where('user_id', Auth::user()->id)
            ->get();

        //Retornamos la información de las cuentas listadas.
        return response()->json($cuentas);
    }

    public function show($cuenta_id)
    {
        //Datos detallados de la cuenta si tiene prestamos.
        $cuenta = Cuenta::find($cuenta_id);

        //Listamos los prestamos asociados a la cuenta del cliente.
        $prestamos = Prestamo::where('cuenta_id', $cuenta->id)
            ->get();

        //Retornamos el objeto con los datos de la cuenta y de los prestamos.
        return response()->json([
            'cuenta' => $cuenta,
            'prestamos' => $prestamos
        ]);
    }
}
