<?php

namespace App\Http\Controllers\Api;

use App\Models\Api\Prestamo;
use Illuminate\Http\Request;
use App\Models\Api\HistorialPago;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class HistorialPagoController extends Controller
{
    public function index($prestamo_id)
    {
        //Obtenemos todo el historial de pago del prestamo.
        $historial = HistorialPago::where('user_id', Auth::user()->id)
            ->where('prestamo_id', $prestamo_id)
            ->get();

        return response()->json([
            'historial_pago' => $historial
        ]);
    }

    public function setPago(Request $request, $prestamo_id)
    {
        //Validación del formulario.
        $validation =  Validator::make($request->all(), [
            'monto_pagado' => ['required'],
        ]);

        //Si hay algún error en la validación, retorna los errores.
        if ($validation->fails()) {
            return response()->json($validation->errors(), 500);
        }


        //Obtenemos los datos del prestamo.
        $prestamo = Prestamo::where('user_id', Auth::user()->id)
            ->where('id', $prestamo_id)
            ->first();

        //Validamos si el estatus es true.
        if ($prestamo->status) {
            //Obtenemos todos los pagos asociados al prestamo en caso de tener.
            $historial = HistorialPago::where('user_id', Auth::user()->id)
                ->where('prestamo_id', $prestamo->id)
                ->get();

            //Agregamos todos los monto pagados a un array y luego sumamos el contenido del array.
            $total_pagado = [];
            foreach ($historial as $item) {
                $total_pagado[] = $item->monto_pagado;
            }

            //Validamos si el total pagado es menor al saldo del prestamo.
            if (array_sum($total_pagado) < $prestamo->saldo_prestamo) {
                $saldo_restante = (array_sum($total_pagado) + $request->monto_pagado);

                //Si el total pagado es menor al saldo del prestamo, creamos un nuevo registro con el abono.
                HistorialPago::create([
                    'user_id' => Auth::user()->id,
                    'prestamo_id' => $prestamo_id,
                    'monto_pagado' => $request->monto_pagado,
                    'saldo_restante' => $prestamo->saldo_prestamo - $saldo_restante,
                    'fecha_pago' => date('Y-m-d')
                ]);

                //Hacemos el calculo para verificar si el total pagado + el monto_pagado es igual al saldo del prestamo para cambiar el estatus.
                if((array_sum($total_pagado) + $request->monto_pagado) == $prestamo->saldo_prestamo) {
                    $prestamo->update([
                        'status' => false
                    ]);
                }

                //Retornamos el mensaje de pago recibido.
                return response()->json([
                    'message' => '¡Pago recibido, muchas gracias!',
                ]);
            }
        } else {
            //Retornamos el siguiente mensaje si el estatus se encuentra en false.
            return response()->json([
                'message' => '¡Este prestamo ya fue pagado en su totalidad!',
            ]);
        }
    }
}
