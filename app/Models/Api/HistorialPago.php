<?php

namespace App\Models\Api;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HistorialPago extends Model
{
    use HasFactory;

    protected $table = 'historial_pago';

    protected $fillable = [
        'user_id',
        'prestamo_id',
        'monto_pagado',
        'saldo_restante',
        'fecha_pago'
    ];
}
