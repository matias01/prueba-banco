<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Configuraciones de la API

1. Instalar las dependencias de Laravel.

`composer install`

2. Configurar el archivo .env a la base de datos para la migración.

3. Ejecutar las migraciones y seeders a la tabla previamente configurada en el .env.

`php artisan migrate:refresh --seed`

** La API cuenta con dos registros de ejemplo, puede utilizarlos de ser necesario para el login **

{
	"email": "john@bank.com",
    "password": "john12345"
}

4. Lanzar servidor.

`php artisan serve`

## Rutas API

1. Registro de usuario tipo POST.

`http://localhost:8000/api/register`

Parametros a enviar:
{
	"name",
	"email",
	"password",
	"answer"
}

2. Login de usuario tipo POST.

`http://localhost:8000/api/login`

Parametros a enviar:
{
	"email",
	"password",
}

** NOTA: Una vez inciada la sesión del usuario, para acceder a las rutas listadas abajo, la API cuenta con token de seguridad Bearer.
** Configurar el cliente POSTMAN con la cabecera correspondiente para el envío del token.

3. Cambio de contraseña tipo POST.

`http://localhost:8000/api/reset-password`

Parametros a enviar:
{
	"email",
	"answer"
	"new_password",
}

4. Listado de cuentas asociadas al cliente tipo GET.

`http://localhost:8000/api/cuentas`

5. Detalle de la cuenta y prestamos asociados tipo GET.

`http://localhost:8000/api/cuenta/{cuenta_id}`

6. Listado total de prestamos asociados a la cuenta tipo GET.

`http://localhost:8000/api/prestamos/{cuenta_id}`

7 Solicitar nuevo prestamo a la cuenta tipo POST.

`http://localhost:8000/api/prestamo/solicitar/{cuenta_id}`

Parametros a enviar:
{
	"saldo_prestamo",
}

8. Detalle del prestamo e historial de pagos asociados al prestamo tipo GET.

`http://localhost:8000/api/historial-pagos/{prestamo_id}`

9. Agregar un pago al prestamo tipo POST.

`http://localhost:8000/api/historial-pagos/pagar/{prestamo_id}`

Parametros a enviar:
{
	"monto_pagado",
}