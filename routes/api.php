<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\CuentaController;
use App\Http\Controllers\Api\Auth\AuthController;
use App\Http\Controllers\Api\Auth\ResetPasswordController;
use App\Http\Controllers\Api\HistorialPagoController;
use App\Http\Controllers\Api\PrestamoController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Register Route */
Route::post('/register', [AuthController::class, 'register']);

/* Login Route */
Route::post('/login', [AuthController::class, 'login']);

/* Reset Password Route */
Route::post('/reset-password', [ResetPasswordController::class, 'resetPassword']);

/* Private Routes */
Route::group(['middleware' => 'auth:sanctum'], function () {
    /* Logout Route */
    Route::get('/logout', [AuthController::class, 'logout']);

    /* Cuentas Route */
    Route::get('/cuentas', [CuentaController::class, 'index']);
    Route::get('/cuenta/{cuenta_id}', [CuentaController::class, 'show']);

    /* Prestamos Route */
    Route::get('/prestamos/{cuenta_id}', [PrestamoController::class, 'getPrestamo']);
    Route::post('/prestamo/solicitar/{cuenta_id}', [PrestamoController::class, 'setPrestamo']);

    /* Historial Pago Routes */
    Route::get('/historial-pagos/{prestamo_id}', [HistorialPagoController::class, 'index']);
    Route::post('/historial-pagos/pagar/{prestamo_id}', [HistorialPagoController::class, 'setPago']);
});
