<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historial_pago', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id'); //Relación del ID a la tabla de usuarios.
            $table->foreign('user_id')->references('id')->on('users')->onDelete('CASCADE');
            $table->unsignedBigInteger('prestamo_id'); //Relación del ID a la tabla de prestamos.
            $table->foreign('prestamo_id')->references('id')->on('prestamos')->onDelete('CASCADE');
            $table->decimal('monto_pagado', 12,2)->default(0); //Monto abonado al prestamo.
            $table->decimal('saldo_restante', 12,2)->default(0); //Monto restante del prestamo.
            $table->date('fecha_pago'); //Fecha del pago abonado.
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historial_pago');
    }
};
