<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuentas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id'); //Relación del ID a la tabla de usuarios.
            $table->foreign('user_id')->references('id')->on('users')->onDelete('NO ACTION');
            $table->string('numero_cuenta')->unique(); //Número de cuenta del cliente.
            $table->string('tipo_cuenta'); //Tipo de cuenta del cliente.
            $table->decimal('saldo_cuenta', 12,2)->default(0); //Saldo actual de la cuenta del cliente.
            $table->boolean('status')->default(true); //Estatus de la cuenta true como ativa, false como inactiva.
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuentas');
    }
};
