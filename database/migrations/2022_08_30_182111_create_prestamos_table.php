<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prestamos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id'); //Relación del ID a la tabla de usuarios.
            $table->foreign('user_id')->references('id')->on('users')->onDelete('CASCADE');
            $table->unsignedBigInteger('cuenta_id'); //Relación del ID a la tabla de cuentas.
            $table->foreign('cuenta_id')->references('id')->on('cuentas')->onDelete('NO ACTION');
            $table->decimal('saldo_prestamo', 12,2); //Monto total del prestamo.
            $table->date('fecha_prestamo'); //Fecha en la cual fue solicitado el prestamo.
            $table->boolean('status')->default(true); //Estatus actual del prestamo true si aún esta activo y false si ya ha sido pagado.
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prestamos');
    }
};
