<?php

namespace Database\Seeders;

use App\Models\Api\Prestamo;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PrestamosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $prestamos = [
            [
                'user_id' => 1,
                'cuenta_id' => 1,
                'saldo_prestamo' => 10000.00,
                'fecha_prestamo' => '2022/03/22',
                'status' => true,
            ],
            [
                'user_id' => 2,
                'cuenta_id' => 3,
                'saldo_prestamo' => 15000.00,
                'fecha_prestamo' => '2020/01/23',
                'status' => false,
            ],
        ];

        foreach ($prestamos as $prestamo) {
            Prestamo::create($prestamo);
        }
    }
}
