<?php

namespace Database\Seeders;

use App\Models\Api\Cuenta;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class CuentasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cuentas = [
            [
                'user_id' => 1,
                'numero_cuenta' => '852456753',
                'tipo_cuenta' => 'Ahorro',
                'saldo_cuenta' => 12500.25,
                'status' => true,
            ],
            [
                'user_id' => 1,
                'numero_cuenta' => '963951357',
                'tipo_cuenta' => 'Nómina',
                'saldo_cuenta' => 0.00,
                'status' => false,
            ],
            [
                'user_id' => 2,
                'numero_cuenta' => '741951861',
                'tipo_cuenta' => 'Corriente',
                'saldo_cuenta' => 54267.57,
                'status' => true,
            ],
        ];

        foreach ($cuentas as $cuenta) {
            Cuenta::create($cuenta);
        }
    }
}
