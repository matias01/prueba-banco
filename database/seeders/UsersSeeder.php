<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'John Doe',
                'email' => 'john@bank.com',
                'password' => Hash::make('john12345'),
                'answer' => 'Hello World'
            ],

            [

                'name' => 'Jane Doe',
                'email' => 'jane@bank.com',
                'password' => Hash::make('jane12345'),
                'answer' => 'World Hello'
            ]
        ];

        foreach ($users as $user) {
            User::create($user);
        }
    }
}
