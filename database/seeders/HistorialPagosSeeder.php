<?php

namespace Database\Seeders;

use App\Models\Api\HistorialPago;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class HistorialPagosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pagos = [
            [
                'user_id' => 1,
                'prestamo_id' => 1,
                'monto_pagado' => 5000.00,
                'saldo_restante' => 5000.00,
                'fecha_pago' => '2022/05/16'
            ],
            [
                'user_id' => 2,
                'prestamo_id' => 2,
                'monto_pagado' => 2000.00,
                'saldo_restante' => 13000.00,
                'fecha_pago' => '2020/02/15'
            ],
            [
                'user_id' => 2,
                'prestamo_id' => 2,
                'monto_pagado' => 3000.00,
                'saldo_restante' => 10000.00,
                'fecha_pago' => '2020/03/23'
            ],
            [
                'user_id' => 2,
                'prestamo_id' => 2,
                'monto_pagado' => 2000.00,
                'saldo_restante' => 8000.00,
                'fecha_pago' => '2020/04/05'
            ],
            [
                'user_id' => 2,
                'prestamo_id' => 2,
                'monto_pagado' => 8000.00,
                'saldo_restante' => 0.00,
                'fecha_pago' => '2020/05/19'
            ],
        ];

        foreach ($pagos as $pago) {
            HistorialPago::create($pago);
        }
    }
}
